package com.example.server.service;

import java.util.List;

import com.example.server.domain.Database;
import com.example.server.domain.Music;
import com.example.server.domain.Video;

public class CustomerService {

	public List<Music> listOfMusic() {
		// TODO Auto-generated method stub
		return Database.getMusicList();
	}

	public Music aMusic(String name) {
		return Database.getAMusic(name);
	}

	public List<Video> listOfVideo() {
		// TODO Auto-generated method stub
		return Database.getVideoList();
	}

	public Video aVideo(String name) {
		return Database.getAVideo(name);
	}
}
