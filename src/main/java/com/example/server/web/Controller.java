package com.example.server.web;


	import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.server.domain.FileStorageService;
import com.example.server.domain.Music;
import com.example.server.domain.Video;
import com.example.server.service.CustomerService;


	@org.springframework.stereotype.Controller
	public class Controller {
		
		@Autowired
		private FileStorageService fileStorageService;
		private String songsPath="C:\\wamp64\\www\\songs";
		
		@RequestMapping(value = "/java/songs", method = RequestMethod.GET)
		@ResponseStatus(HttpStatus.OK)
		@ResponseBody
		public List<Music> listOfMusic(){
			return new CustomerService().listOfMusic();
		}
		
		@RequestMapping(value = "/java/songs/{name}", method = RequestMethod.GET)
		@ResponseStatus(HttpStatus.OK)
		@ResponseBody
		public Music aMusic(@PathVariable("name") String name) throws Exception{
			return new CustomerService().aMusic(name);
		}
		
		@RequestMapping(value = "/java/videos", method = RequestMethod.GET)
		@ResponseStatus(HttpStatus.OK)
		@ResponseBody
		public List<Video> listOfVideo(){
			return new CustomerService().listOfVideo();
		}
	
		@RequestMapping(value = "/java/videos/{name}", method = RequestMethod.GET)
		@ResponseStatus(HttpStatus.OK)
		@ResponseBody
		public Video aVideo(@PathVariable("name") String name) throws Exception{
			return new CustomerService().aVideo(name);
		}
		

		@RequestMapping(value = "/java/songs/download/{name}", method = RequestMethod.GET)
		@ResponseStatus(HttpStatus.OK)
		@ResponseBody
		public ResponseEntity<Resource> downloadMusic(@PathVariable("name") String fileName, HttpServletRequest request) throws Exception{
	        // Load file as Resource
	        Resource resource = fileStorageService.loadFileAsResource(songsPath+"\\"+fileName);

	        // Try to determine file's content type
	        String contentType = null;
	        try {
	            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
	        } catch (IOException ex) {
	            System.out.println("Could not determine file type.");
	        }

	        // Fallback to the default content type if type could not be determined
	        if(contentType == null) {
	            contentType = "application/octet-stream";
	        }

	        return ResponseEntity.ok()
	                .contentType(MediaType.parseMediaType(contentType))
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
	                .body(resource);
	    
		}

}
