package com.example.server.domain;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class Database {
	private static Connection c;
	private static final String DB_ADDRESS = "127.0.0.1";
	private static final String DB_PORT = "3306";
	//private static final String DB_PATH = "/db/M1Project";
	private static final String DB_NAME = "sleep_music";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "";
	static {
		try {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://"+DB_ADDRESS+":"+DB_PORT+"/"+DB_NAME;
		c = DriverManager.getConnection(url, DB_USER,DB_PASSWORD);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		}/*
		musicList = new ArrayList<>();
		videoList = new ArrayList<>();
		musicList.add(new Music("S001.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S002.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S003.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S004.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S005.mp3","Artist_5", "2014", "Length_5",""));
		musicList.add(new Music("S006.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S007.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S008.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S009.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S010.mp3","Artist_5", "2014", "Length_5",""));
		musicList.add(new Music("S011.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S012.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S013.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S014.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S015.mp3","Artist_5", "2014", "Length_5",""));
		musicList.add(new Music("S016.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S017.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S018.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S019.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S020.mp3","Artist_5", "2014", "Length_5",""));
		musicList.add(new Music("S021.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S022.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S023.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S024.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S025.mp3","Artist_5", "2014", "Length_5",""));
		musicList.add(new Music("S026.mp3","Artist_1", "2018", "Length_1",""));
		musicList.add(new Music("S027.mp3","Artist_2", "2016", "Length_2",""));
		musicList.add(new Music("S028.mp3","Artist_3", "2017", "Length_3",""));
		musicList.add(new Music("S029.mp3","Artist_4", "2012", "Length_4",""));
		musicList.add(new Music("S030.mp3","Artist_5", "2014", "Length_5",""));
		videoList.add(new Video("V001.mp4","Artist_1", "2018", "Length_1",""));
		videoList.add(new Video("V002.mp4","Artist_2", "2016", "Length_2",""));
		videoList.add(new Video("V003.mp4","Artist_3", "2017", "Length_3",""));
		videoList.add(new Video("V004.mp4","Artist_4", "2012", "Length_4",""));
		videoList.add(new Video("V005.mp4","Artist_5", "2014", "Length_5",""));
		videoList.add(new Video("V006.mp4","Artist_1", "2018", "Length_1",""));
		videoList.add(new Video("V007.mp4","Artist_2", "2016", "Length_2",""));
		videoList.add(new Video("V008.mp4","Artist_3", "2017", "Length_3",""));
		videoList.add(new Video("V009.mp4","Artist_4", "2012", "Length_4",""));
		videoList.add(new Video("V010.mp4","Artist_5", "2014", "Length_5",""));
		videoList.add(new Video("V011.mp4","Artist_1", "2018", "Length_1",""));
		videoList.add(new Video("V012.mp4","Artist_2", "2016", "Length_2",""));
		videoList.add(new Video("V013.mp4","Artist_3", "2017", "Length_3",""));
		videoList.add(new Video("V014.mp4","Artist_4", "2012", "Length_4",""));
		videoList.add(new Video("V015.mp4","Artist_5", "2014", "Length_5",""));
		videoList.add(new Video("V016.mp4","Artist_1", "2018", "Length_1",""));
		videoList.add(new Video("V017.mp4","Artist_2", "2016", "Length_2",""));
		videoList.add(new Video("V018.mp4","Artist_3", "2017", "Length_3",""));
		videoList.add(new Video("V019.mp4","Artist_4", "2012", "Length_4",""));
		videoList.add(new Video("V020.mp4","Artist_5", "2014", "Length_5",""));
		videoList.add(new Video("V021.mp4","Artist_1", "2018", "Length_1",""));
		videoList.add(new Video("V022.mp4","Artist_2", "2016", "Length_2",""));
		videoList.add(new Video("V023.mp4","Artist_3", "2017", "Length_3",""));
		videoList.add(new Video("V024.mp4","Artist_4", "2012", "Length_4",""));
		videoList.add(new Video("V025.mp4","Artist_5", "2014", "Length_5",""));
		*/}
	
	public static List<Music> getMusicList() {
		try {
			String query = "SELECT * "
						 + "FROM media_info "
						 + "WHERE media_type = 'music';";
			
			ArrayList<Music> musicList = new ArrayList<>();
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				String songName = rs.getString("media_name");
				musicList.add(new Music(songName));
			}
			
			rs.close();
			stmt.close();
			return musicList;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Music getAMusic(String name) {
		try {
			String query = "SELECT * "
						 + "FROM media_info "
						 + "WHERE media_name = '"+name+"' "
						 + "AND media_type = 'music';";
			
			Music music = null;
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			if (rs.next()) {
				String songName = rs.getString("media_name");
				music = new Music(songName);
			}
			
			rs.close();
			stmt.close();
			return music;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Video getAVideo(String name) {
		try {
			String query = "SELECT * "
						 + "FROM media_info "
						 + "WHERE media_name = '"+name+"' "
						 + "AND media_type = 'video';";
			
			Video video = null;
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			if (rs.next()) {
				String videoName = rs.getString("media_name");
				video = new Video(videoName);
			}
			
			rs.close();
			stmt.close();
			return video;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<Video> getVideoList() {
		try {
			String query = "SELECT * "
						 + "FROM media_info "
						 + "WHERE media_type = 'video';";
			
			ArrayList<Video> videoList = new ArrayList<>();
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				String songName = rs.getString("media_name");
				videoList.add(new Video(songName));
			}
			
			rs.close();
			stmt.close();
			return videoList;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
