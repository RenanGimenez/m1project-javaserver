package com.example.server.domain;

public class Music {

	private String name;
	private String artist;
	private String year;
	private String length;
	private String uri;

	public Music(String name, String artist, String year, String length, String uri) {
		this.name = name;
		this.artist = artist;
		this.year = year;
		this.length = length;
		this.uri = uri;
	}
	
	public Music(String songName) {
		this(songName, "","","","");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
